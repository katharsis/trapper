var serializeObject = function(element){
    var obj = {};
    $.each( element.serializeArray(), function(i,o){
	var n = o.name,
	v = o.value;
	obj[n] = obj[n] === undefined ? v
	    : $.isArray( obj[n] ) ? obj[n].concat( v )
	    : [ obj[n], v ];
    });  
    return obj;
};


