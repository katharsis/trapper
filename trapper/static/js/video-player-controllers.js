var player

http://stackoverflow.com/questions/8043026/javascript-format-number-to-have-2-digit
function leftPad(number, targetLength) {
    var output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
}

// http://stackoverflow.com/questions/6312993/javascript-seconds-to-time-with-format-hhmmss
function secondsToTime(secs) {
    secs = Math.round(secs);
    var hours = Math.floor(secs / (60 * 60));
    var divisor_for_minutes = secs % (60 * 60);
    var minutes = Math.floor(divisor_for_minutes / 60);
    var divisor_for_seconds = divisor_for_minutes % 60;
    var seconds = Math.ceil(divisor_for_seconds);
    return leftPad(hours,2) + ':' + leftPad(minutes,2) + ':' + leftPad(seconds,2);
}

function timeToSeconds(time) {
    var timeS = time.split(':')
    return parseInt(timeS[0])*3600 + parseInt(timeS[1])*60 + parseInt(timeS[2])
}

function playLoop(elem) {
    var start = $(elem).parent().parent().find('input.timeinput')[0].value
    var end = $(elem).parent().parent().find('input.timeinput')[1].value
    player.play();
    setTimeout(function(){
	player.loopBetween(timeToSeconds(start), timeToSeconds(end));
    }, 100)
}

function getValues(elem) {
    var values = player.getValueSlider();
    $(elem).parent().parent().find('input.timeinput')[0].value = secondsToTime(videojs.round(values.start,2));
    $(elem).parent().parent().find('input.timeinput')[1].value = secondsToTime(videojs.round(values.end,2));
}

function setValues(elem) {
    var start = $(elem).parent().parent().find('input.timeinput')[0].value
    var end = $(elem).parent().parent().find('input.timeinput')[1].value                  
    player.setValueSlider(timeToSeconds(start), timeToSeconds(end));
}                                     

function playLoopReadOnly(start, end) {
    player.play();
    setTimeout(function(){
	player.loopBetween(timeToSeconds(start), timeToSeconds(end));
    }, 100)
}

$(window).load(function() {
    player = videojs($('#videoPlayer')[0])
    player.rangeslider(
	{
	    hidden:false,
	    locked:false,
	    panel:false,
	} 
    );
});

