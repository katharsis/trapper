var clearSelection = function() {
    $('#sequence-detail').empty();
    $('#sequence-update-button').empty();
    $('#sequence-delete-button').empty();
    $('li.ui-selectee').removeClass('ui-selected');
    $('li.ui-selectee div.sequences-items').removeClass('sequences-items-selected');
}

// update/create forms
var getSequenceForm = function(url) {
    $.get(url, function(data) {
	$('#modal-form-alert').addClass('hide');
	$('#modal-form').html(data['form_html']);
	$('#modal-form-title').html(data['msg']);
    })
	.done(function() {
	    $('#modal-form-submit-button').html(
		'<a href="#" class="btn btn-success" role="button" onclick="submitSequenceForm(\''+url+'\')">Submit</a>'
	    )
	    $('#modal-form-template').modal("show");
	})
	.fail(function(data) {
	    alert(data['status']+': '+data['statusText'])
	})
}

var submitSequenceForm = function(url) {
    var ids = []
    $( "li.ui-selected").each(function(){ids.push($(this).attr('id'))});
    $('form[id=sequence-form] #id_resources').val(ids)
    $.post(url, 
	   $('#sequence-form').serializeArray(),
	   function(data) {
	       $('#modal-form').empty()
	       $('#modal-form-alert').removeClass('hide').addClass('alert-success');
	       $('#modal-form-alert-msg').html(data['msg']);
	   })
	.done(function() {
	    refreshSequences(sequencesUrl);
	    clearSelection();
	})
	.fail(function(data) {
	    $('#modal-form').html(data.responseJSON['form_html']);
	})
}

var createSequence = function(collectionPk) {
    var url = '/media_classification/sequence/create/'+collectionPk+'/'
    getSequenceForm(url);
}

var updateSequence = function(pk) {
    var url = '/media_classification/sequence/update/'+pk+'/'
    getSequenceForm(url);
}

var deleteSequence = function(pk) {
    var url = '/media_classification/sequence/delete/'+pk+'/'
    $.confirm({
	text: "Are you sure you want to delete that sequence?",
	confirm: function(button) {
	    $.get(url, function(data) {
	    })
    		.done(function() {
		    clearSelection();
		    refreshSequences(sequencesUrl);
		})
		.fail(function(data) {
		    alert(data['status']+': '+data['statusText'])
		})
	},
	cancel: function(button) {
	}	
    });
}

var viewSequence = function(pk) {
    clearSelection();
    var url = '/media_classification/sequence/detail/'+pk+'/'
    $.get(url, function(data) {
	$('#sequence-detail').html(data['html']);
    })
	.done(function(data) {
	    if (data['can_edit']) {
		$('#sequence-update-button').html(
		    '<a href="#" class="btn btn-default btn-xs" role="button" onclick="updateSequence('+pk+')">Update</a>'
		);
		$('#sequence-delete-button').html(
		    '<a href="#" class="btn btn-danger btn-xs" role="button" onclick="deleteSequence('+pk+')">Delete</a>'
		);
	    };
	    var selector = 'li#'+data['resources'].join(',li#');
	    $(selector).addClass('ui-selected');
	    var selector = 'li#'+data['resources'].join(' div.sequence-'+pk+',li#')+' div.sequence-'+pk;
	    $(selector).addClass('sequences-items-selected');
	})
	.fail(function(data) {
	    alert(data['status']+': '+data['statusText'])
	})
}

var refreshSequences = function(url) {
    $.get(url, function(data) {
	$('div.sequences-current-resource').empty();
	for (var property in data) {
	    $('div.sequences-current-resource').append(
		'<div class="sequence sequence-'+property+'"></div>'
	    )
	    var selector = 'li#'+data[property].join(' div.sequence-'+property+', li#')+' div.sequence-'+property
	    $(selector).addClass('sequences-items')
	    $(selector).append('<a href="#" class="white" onclick="viewSequence('+property+')"><i class="icon-expand"></i></a>')
	}	
    })
 	.done(function() {
	})
	.fail(function(data) {
	    alert(data['status']+': '+data['statusText'])
	})
}
    
$(function(){
    $('#selection-clear').click(function() {
	event.preventDefault();
	clearSelection();
    });
    $('#sequence-create').click(function() {
	event.preventDefault();
	createSequence(collectionPk);
    });
}); 
