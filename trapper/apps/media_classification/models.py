from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django import forms
from django.conf import settings
from django.db.models import get_model

from trapper.apps.storage.models import Resource
from trapper.apps.research.models import Project as RProject, ProjectCollection as RProjectCollection
from django_select2.fields import AutoModelSelect2Field

from django_hstore import hstore

import json
from operator import itemgetter
from collections import OrderedDict


class Project(models.Model):
    """Describes a single classification project existing withing the system
    """

    name = models.CharField(max_length=255)
    research_project = models.ForeignKey(RProject, related_name='classification_projects')
    collections = models.ManyToManyField(RProjectCollection, through='ProjectCollection', blank=True, null=True, related_name="classifications")
    """Collections assigned to the project"""
    createdby = models.ForeignKey(User, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, default='2014-01-01 12:00:00')
    sequencing_enabled = models.BooleanField("Sequences", default=True, help_text="Enable sequencing interface")
    cs_enabled = models.BooleanField(default=True)
    """Is crowd-sourcing enabled for the project ?"""

    def __unicode__(self):
        return unicode(self.name)

    def get_all_cs_resources(self):
        """Returns a list of crowd-sourcing enabled resources for given project."""

        resources = []

        if not self.cs_enabled:
            return resources

        for c in self.projectcollection_set.filter(cs_enabled=True):
            new = [r for r in c.collection.resources.all()]
            resources.extend(new)
        return list(set(resources))

    def is_project_admin(self, user):
        role = self.cproject_roles.filter(user=user)
        return len(role) > 0 and role[0].name == 'A'

    # TODO: given user can have only one role in a project; rewrite tests below
    def determine_roles(self, user):
        """Returns a tuple of project roles for given user.

        :param user: user for which the roles are determined
        :type user: :py:class:`django.contrib.auth.models.User`
        :return: list of role names of given user withing the project
        :rtype: str
        """

        return [r.name for r in self.cproject_roles.filter(user=user)]

    def can_update(self, user):
        """Determines whether given user can update the project.

        :param user: user for which the test is made
        :type user: :py:class:`django.contrib.auth.models.User`
        :return: True if user can edit the project, False otherwise
        :rtype: bool
        """

        return self.cproject_roles.filter(user=user, name__in=ProjectRole.ROLE_EDIT).count() > 0

    def can_detail(self, user):
        """Determines whether given user can see the details of a project.

        :param user: user for which the check is made
        :type user: :py:class:`django.contrib.auth.models.User`
        :return: True if user can see the details of the project, False otherwise
        :rtype: bool
        """

        return self.cproject_roles.filter(user=user).count() > 0

    def get_absolute_url(self):
        return reverse('media_classification:project_detail', kwargs={'pk':self.pk})


class ProjectCollection(models.Model):
    """Many-To-Many model for Project-Collection relationship."""

    project = models.ForeignKey(Project)
    collection = models.ForeignKey(RProjectCollection)
    active = models.BooleanField("Active", default=True)
    """Is collection "active" within the project at given moment ?"""
    group_by_location = models.BooleanField("Group resources by location", default=False)
    """Should resources for this collection be grouped by location in classification view?"""
    sequencing_experts = models.BooleanField("Sequences editable by experts", default=True, help_text="Allow experts to create and edit sequences")
    cs_enabled = models.BooleanField("Crowd-Sourcing", default=True, help_text="Enable Crowd-Sourcing")
    """Is collection available for the crowd-sourcing ?"""

    def __unicode__(self):
        return unicode("%s" % self.collection)

    def get_resources(self):
        return self.collection.collection.resources.all().order_by('date_recorded', 'name')

    def get_resources_with_status(self):
        resources = self.get_resources()
        resources_pks = resources.values_list('pk', flat=True)
        status_list = Classification.objects.filter(
            project__pk=1, resource__pk__in=resources_pks
        ).values_list('approved', flat=True).order_by('resource__date_recorded', 'resource__name')
        return zip(resources, status_list)

#---------------------------------------------------------------#


class AttributeSet(models.Model):
    """Defined group of attributes describing given classification form.
    Such set is defined per-resource type, and is defined in given classification project.
    """

    CUSTOM_ATTRS_FIELDS = {
        'B': forms.BooleanField,
        'F': forms.FloatField,
        'I': forms.IntegerField,
        'S': forms.CharField,
    }

    name = models.CharField(max_length=255)
    project = models.OneToOneField(Project, verbose_name='Media classification project', null=True, blank=True, related_name="attributeset")
    predefined_attrs = hstore.DictionaryField(null=True, blank=True)
    custom_attrs = hstore.DictionaryField(null=True, blank=True)
    dynamic_attrs_order = models.TextField(null=True, blank=True)
    static_attrs_order = models.TextField(null=True, blank=True)
    owner = models.ForeignKey(User, null=True, blank=True, related_name='owned_attsets')

    objects = hstore.HStoreManager()

    def __unicode__(self):
        return unicode("Name: %s | Project: %s" % (self.name, self.project.name))

    def get_absolute_url(self):
        return reverse('media_classification:attributeset_detail', kwargs={'pk':self.pk})

    def can_edit(self, user):
        return self.owner == user

    def get_pre_attrs_names(self):
        return settings.PREDEFINED_ATTRIBUTES_SIMPLE.keys() + settings.PREDEFINED_ATTRIBUTES_MODELS.keys()

    def get_active_pre_attrs_names(self):
        config = self.get_pre_attrs_names()
        active = []
        for attr in config:
            if self.predefined_attrs[attr] == 'true':
                active.append(attr)
        return active

    def get_all_attrs_names(self):
        all = [[],[]] # [[dynamic_form], [static_form]]
        for at in self.get_active_pre_attrs_names():
            if self.predefined_attrs['target_%s' % at] == 'D':
                all[0].append(at)
            else:
                all[1].append(at)
        for at in self.custom_attrs.keys():
            if json.loads(self.custom_attrs[at])['target'] == 'D':
                all[0].append(at)
            else:
                all[1].append(at)
        return all

    def get_dynamic_attrs_order(self):
        try:
            return self.dynamic_attrs_order.split(',')
        except:
            return []

    def get_static_attrs_order(self):
        try:
            return self.static_attrs_order.split(',')
        except:
            return []

    def update_attrs_order(self):
        all_attrs_names = self.get_all_attrs_names()
        dynamic_attrs_order = self.get_dynamic_attrs_order()
        static_attrs_order = self.get_static_attrs_order()
        ordered_attrs = []
        new_attrs = []
        for at in all_attrs_names[0]: # dynamic_form
            if not at in dynamic_attrs_order:
                dynamic_attrs_order.append(at)
        for at in dynamic_attrs_order:
            if not at in all_attrs_names[0]:
                del dynamic_attrs_order[dynamic_attrs_order.index(at)]
        for at in all_attrs_names[1]: # static_form
            if not at in static_attrs_order:
                static_attrs_order.append(at)
        for at in static_attrs_order:
            if not at in all_attrs_names[1]:
                del static_attrs_order[static_attrs_order.index(at)]
        self.dynamic_attrs_order = ','.join(dynamic_attrs_order)
        self.static_attrs_order = ','.join(static_attrs_order)
        self.save()

    def parse_hstore_values(self, field, attr=None):
        parsed_values = {}
        # if attr not specified parse all
        if not attr:
            items = getattr(self, field).items()
        else:
            items = [(attr, getattr(self, field)[attr], ),]
        for k,v in items:
            try:
                parsed_values[k] = json.loads(v)
            except ValueError:
                # if it is not a json it must be a simple string so just add it to a parsed_values dict
                parsed_values[k] = v
        return parsed_values

    def prepare_class_form_fields_defs(self):
        # parsed custom attributes
        ca = self.parse_hstore_values('custom_attrs')
        # parsed predefined attributes
        pa = self.parse_hstore_values('predefined_attrs')
        form_fields = OrderedDict()
        form_fields['D'] = OrderedDict() # "D" -> dynamic form
        form_fields['S'] = OrderedDict() # "S" -> dynamic form
        # first prepare predefined attributes:
        pas = settings.PREDEFINED_ATTRIBUTES_SIMPLE
        pam = settings.PREDEFINED_ATTRIBUTES_MODELS
        for key in pas.keys():
            if pa.get(key):
                if pas[key].get('widget'):
                    widget = pas[key]['widget']
                    formfield = pas[key]['formfield'](required=pa['required_%s' % key] ,widget=widget)
                else:
                    formfield = pas[key]['formfield'](required=pa['required_%s' % key])
                form_fields[pa['target_%s' % key]][key] = formfield
        for key in pam.keys():
            if pa.get(key):
                model = get_model(pam[key]['app'], key)
                if pa['selected_%s' % key]:
                    query = model.objects.filter(pk__in=pa['selected_%s' % key])
                    choices = query.values_list(pam[key]['choices_labels'], pam[key]['choices_labels'])
                    formfield = forms.ChoiceField(choices=choices, required=pa['required_%s' % key])
                    form_fields[pa['target_%s' % key]][key] = formfield
                else:
                    formfield = AutoModelSelect2Field(
                        queryset = model.objects,
                        search_fields = pam[key]['search_fields'],
                        required = pa['required_%s' % key],
                    )
                    form_fields[pa['target_%s' % key]][key] = formfield
        # next prepare custom attributes:
        for key in ca.keys():
            if ca[key]['values']:
                # values have been already checked during a form validation so just split them
                splitted_values = ca[key]['values'].split(',')
                choices = zip(splitted_values, splitted_values)
                formfield = forms.ChoiceField(choices=choices, required=ca[key]['required'], initial=ca[key]['initial'])
                form_fields[ca[key]['target']][key] = formfield
            else:
                formfield = self.CUSTOM_ATTRS_FIELDS[ca[key]['at_type']](required=ca[key]['required'], initial=ca[key]['initial'],)
                form_fields[ca[key]['target']][key] = formfield
        return form_fields


#---------------------------------------------------------------#
# CLASSIFICATION MODELS (expert decisions)
#---------------------------------------------------------------#

# helper function, move to tools module
def get_ordered_values(keys_list, d):
    return [d.get(k) for k in keys_list]


class Classification(models.Model):
    """The result of classification process for given resource.
    .. seealso::
        * :class:`ClassificationUser`
    """

    resource = models.ForeignKey(Resource)
    project = models.ForeignKey(Project)
    pcollection = models.ForeignKey(ProjectCollection)
    approved = models.BooleanField(default=False)
    """is this classification approved by admins of classification project?"""
    classified_by = models.ForeignKey(User, null=True, blank=True, related_name='+')
    approved_by = models.ForeignKey(User, null=True, blank=True, related_name='+')
    approved_date = models.DateTimeField(null=True, blank=True)
    modified_by = models.ForeignKey(User, null=True, blank=True, related_name='+')
    modified_date = models.DateTimeField(null=True, blank=True)
    data_static_form = hstore.DictionaryField(null=True, blank=True)

    objects = hstore.HStoreManager()

    def __unicode__(self):
        return unicode("Classification: %s, Resource: %s" % (self.pk, self.resource))

    def get_attset(self):
        return self.project.attributeset

    def get_users_class(self):
        return self.users_classifications.all().order_by('modified')

    def get_approved_static_values(self):
        static_order = self.get_attset().get_static_attrs_order()
        return get_ordered_values(static_order, self.approved_data_static_form)

    def get_approved_dynamic_rows(self):
        values_list = []
        dynamic_order = self.get_attset().get_dynamic_attrs_order()
        for row in self.data_dynamic_form_rows.values_list('data_dynamic_form_row', flat=True):
            values_list.append(
                get_ordered_values(dynamic_order, row)
            )
        return values_list



class ClassificationDynamicFormRow(models.Model):
    classification = models.ForeignKey(Classification, related_name='data_dynamic_form_rows')
    data_dynamic_form_row = hstore.DictionaryField(null=True, blank=True)

    objects = hstore.HStoreManager()


class ClassificationUser(models.Model):
    """A model to store classifications made by users.
    .. note::
    """

    classification = models.ForeignKey(Classification, related_name="users_classifications")
    data_static_form = hstore.DictionaryField(null=True, blank=True)
    user = models.ForeignKey(User)
    classified = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now_add=True)

    objects = hstore.HStoreManager()

    def __unicode__(self):
        return unicode("%s | User: %s" % (self.classification, self.user.username))

    def get_static_values(self):
        static_order = self.classification.get_attset().get_static_attrs_order()
        return get_ordered_values(static_order, self.data_static_form)

    def get_dynamic_rows(self):
        values_list = []
        dynamic_order = self.classification.get_attset().get_dynamic_attrs_order()
        for row in self.data_dynamic_form_rows.values_list('data_dynamic_form_row', flat=True):
            values_list.append(
                get_ordered_values(dynamic_order, row)
            )
        return values_list

    def get_user_role(self):
        return self.classification.project.cproject_roles.get(user=self.user)


class ClassificationUserDynamicFormRow(models.Model):
    classification_user = models.ForeignKey(ClassificationUser, related_name='data_dynamic_form_rows')
    data_dynamic_form_row = hstore.DictionaryField()

    objects = hstore.HStoreManager()



class Sequence(models.Model):
    """Sequence of resources identified by an expert (ADMIN or EXPERT in a project)."""

    name = models.CharField(max_length=50)
    description = models.TextField(max_length=1000, null=True, blank=True)
    collection = models.ForeignKey(ProjectCollection)
    """ProjectCollection"""
    resources = models.ManyToManyField(Resource)
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, null=True, blank=True)

    def __unicode__(self):
        return unicode("Sequence %s defined by %s" % (self.name, self.user))

    def get_absolute_url(self):
        return reverse('media_classification:sequence_detail', kwargs={'pk':self.pk})


class ProjectRole(models.Model):
    """Model describing the user's role withing given :class:`.Project`"""

    ROLE_PROJECT_ADMIN = "A"
    ROLE_EXPERT = "E"
    ROLE_COLLABORATOR = "C"

    ROLE_ANY = (ROLE_PROJECT_ADMIN, ROLE_EXPERT, ROLE_COLLABORATOR, )
    ROLE_EDIT = (ROLE_PROJECT_ADMIN, ROLE_EXPERT, )

    ROLE_CHOICES = (
        (ROLE_PROJECT_ADMIN, "Admin"),
        (ROLE_EXPERT, "Expert"),
        (ROLE_COLLABORATOR, "Collaborator"),
    )

    user = models.ForeignKey(User)
    """User for which the role is defined"""

    name = models.CharField(max_length=1, choices=ROLE_CHOICES)
    """Role name"""

    project = models.ForeignKey(Project, related_name="cproject_roles")
    """Project for which the role is defined"""

    def __unicode__(self):
        return unicode("%s | Project: %s | Role: %s " % (self.user.username, self.project.name, self.get_name_display()))
