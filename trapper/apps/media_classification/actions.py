from django.http import HttpResponseRedirect

def test_action(view, queryset):
    for c in queryset:
        for row in c.data_dynamic_form_rows.all():
            row.data_dynamic_form_row['comments'] = 'TEST ACTION'
            row.save()
    return HttpResponseRedirect('.')

test_action.short_description = 'Test action'
