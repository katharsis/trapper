from django.core.urlresolvers import reverse
from django.test import TestCase
from django.contrib.auth.models import User

from trapper.apps.media_classification.models import Project, ProjectRole, AttributeSet
from trapper.apps.research.models import Project as RProject


class ProjectViewsTestDBTest(TestCase):

    def setUp(self):
        self.rp0 = RProject.objects.create(name="RProject_0")
        self.u0 = User.objects.create_user('user1', 'user@users.com', 'user1')
        self.p0 = Project.objects.create(name="Project_0", research_project=self.rp0)
        self.p1 = Project.objects.create(name="Project_1", research_project=self.rp0)
        self.pr0 = ProjectRole.objects.create(name=ProjectRole.ROLE_PROJECT_ADMIN, user=self.u0, project=self.p0)

    def test_project_list_anon(self):
        """List of projects is publicly available to anonymous users."""

        response = self.client.get(reverse('media_classification:project_list'))
        self.assertEqual(response.status_code, 200)

    def test_project_detail_anon(self):
        """Anonymous users cannot access the details of any project."""

        response = self.client.get(reverse('media_classification:project_detail', kwargs={'pk': self.p1.pk}))
        self.assertEqual(response.status_code, 302)

    def test_project_detail_forbidden(self):
        """Logged-in user accessing a project he does not participate in."""

        self.client.login(username='user1', password='user1')
        response = self.client.get(reverse('media_classification:project_detail', kwargs={'pk': self.p1.pk}))
        self.assertEqual(response.status_code, 403)

    def test_project_detail_allowed(self):
        """Logged-in user accessing a project he participates in."""

        self.client.login(username='user1', password='user1')
        response = self.client.get(reverse('media_classification:project_detail', kwargs={'pk': self.p0.pk}))
        self.assertEqual(response.status_code, 200)



'''
# create test AttributeSet
custom_attrs = {
    u'SEX': u'{"initial": "unknown", "target": "D", "required": true, "values": "unknown,male,female", "at_type": "S"}',
    u'AGE': u'{"initial": "unknown", "target": "D", "required": true, "values": "unknown,baby,juvenile,adult", "at_type": "S"}',
    u'N': u'{"initial": "1", "target": "D", "required": true, "values": "", "at_type": "I"}',
    u'BEHAVIOR': u'{"initial": "unknown", "target": "D", "required": true, "values": "unknown,vigilance,browsing,grazing,running,rooting", "at_type": "S"}',
    u'EMPTY': u'{"initial": "False", "target": "D", "required": true, "values": "", "at_type": "B"}',
    u'FOREST_TYPE': u'{"initial": "unknown", "target": "S", "required": true, "values": "unknown,DF,CF,MCF,MDF,FPF", "at_type": "S"}',
    u'QUALITY': u'{"initial": "good", "target": "S", "required": true, "values": "terrible,bad,good,excellent", "at_type": "S"}',
}

predefined_attrs = {
    u'annotations': u'true',
    u'filter_species_family': u'[]',
    u'filter_species_genus': u'[]',
    u'selected_species': u'[527, 100, 534, 536, 4849]',
    u'species': u'true'
}

att_set = AttributeSet(name='TEST_VIDEO_EXPERT', resource_type='V', attset_type='E', project=project, custom_attrs=custom_attrs, predefined_attrs=predefined_attrs, owner=user)
'''
