from django import forms
import django_filters
from trapper.apps.media_classification.models import Classification, ProjectCollection
from .forms import ClassificationFilterForm

class ClassificationFilter(django_filters.FilterSet):

    date_recorded_min = django_filters.DateTimeFilter(name='resource__date_recorded', lookup_type='gt', label='Date start')
    date_recorded_max = django_filters.DateTimeFilter(name='resource__date_recorded', lookup_type='lt', label='Date end')
    pcollection = django_filters.MultipleChoiceFilter()

    class Meta:
        model = Classification
        form = ClassificationFilterForm
        fields = ['pcollection', 'approved', 'resource__resource_type', 'date_recorded_min', 'date_recorded_max']
        order_by = (
            ('', '-- Order by --'),
            ('resource__date_recorded', 'Date'),
            ('resource__name', 'Name'),
        )


    def __init__(self, *args, **kwargs):
        if kwargs.get('project'):
            project = kwargs.pop('project')
            attset_fields = project.attributeset.prepare_class_form_fields_defs()
            self.update_filters(attset_fields)
        super(ClassificationFilter, self).__init__(*args, **kwargs)
        # add empty lables
        self.filters['pcollection'].field.choices = [('', '-- Collection --')] + list(ProjectCollection.objects.filter(project=project).values_list('pk', 'collection__collection__name').order_by('collection__collection__name'))
        self.filters['pcollection'].field.widget.attrs['data-placeholder'] = 'Collection'
        self.filters['resource__resource_type'].field.choices.insert(0, ('', '-- All --'))

    def update_filters(self, attset_fields):
        for key in attset_fields.keys():
            for field in attset_fields[key]:
                if type(attset_fields[key][field]) == forms.ChoiceField:
                    self.base_filters[field] = HstoreChoiceFilter(
                        name=field, choices=[('', '-- '+field.capitalize()+ ' --')] + attset_fields[key][field].choices, class_form=key
                    )


class HstoreChoiceFilter(django_filters.filters.Filter):
    field_class = forms.ChoiceField

    def __init__(self, class_form='D', *args, **kwargs):
        self.class_form = class_form
        super(HstoreChoiceFilter, self).__init__(*args, **kwargs)

    def filter(self, qs, value):
        if value in ([], (), {}, None, ''):
            return qs
        else:
            if self.class_form == 'D':
                return qs.filter(
                    data_dynamic_form_rows__data_dynamic_form_row__contains={
                        self.name : value
                        }
                )
            if self.class_form == 'S':
                return qs.filter(
                    data_static_form__contains={
                        self.name : value
                        }
                )


