from django.contrib import admin
from trapper.apps.media_classification.models import AttributeSet, Classification, ClassificationDynamicFormRow, ClassificationUser, ClassificationUserDynamicFormRow, Project, ProjectRole, ProjectCollection, Sequence


class ClassificationDynamicFormRowInline(admin.StackedInline):
    model = ClassificationDynamicFormRow
    extra = 0

class ClassificationUserInline(admin.StackedInline):
    model = ClassificationUser
    extra = 0

class ClassificationAdmin(admin.ModelAdmin):
    inlines = [ClassificationDynamicFormRowInline, ClassificationUserInline,]

class ClassificationUserDynamicFormRowInline(admin.StackedInline):
    model = ClassificationUserDynamicFormRow
    extra = 0

class ClassificationUserAdmin(admin.ModelAdmin):
    inlines = [ClassificationUserDynamicFormRowInline,]

class ProjectRoleInline(admin.TabularInline):
    model = ProjectRole
    extra = 0

class ProjectCollectionInline(admin.TabularInline):
    model = ProjectCollection
    extra = 0

class ProjectAdmin(admin.ModelAdmin):
    inlines = [ProjectRoleInline, ProjectCollectionInline]
    filter_horizontal = ('collections',)

    def get_form(self, request, obj=None, **kwargs):
        form = super(ProjectAdmin, self).get_form(request, obj, **kwargs)
        return form

admin.site.register(AttributeSet)
admin.site.register(ProjectCollection)
admin.site.register(Classification, ClassificationAdmin)
admin.site.register(ClassificationUser, ClassificationUserAdmin)
admin.site.register(ProjectRole)
admin.site.register(Sequence)
admin.site.register(Project, ProjectAdmin)
