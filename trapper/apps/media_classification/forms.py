from django import forms
from django.forms.models import inlineformset_factory

from trapper.apps.media_classification.models import Project, ProjectCollection, ProjectRole, AttributeSet, Sequence

class ProjectForm(forms.ModelForm):
    """Project ModelForm for the Update/Create views"""

    class Meta:
        model = Project
        exclude = ['collections', 'research_project']

    rp_pk = forms.IntegerField(widget=forms.HiddenInput)

class ProjectCollectionForm(forms.ModelForm):
    """ProjectCollection ModelForm for the Update/Create views"""

    class Meta:
        model = ProjectCollection

    def __init__(self, *args, **kwargs):
        super(ProjectCollectionForm, self).__init__(*args,**kwargs)
        #if self.instance.id:
            # Adds collection as a simple label, so the user can't alter it.
         #   self.col_name = self.instance.collection.name
          #  del self.fields['collection']


ProjectCollectionFormset = inlineformset_factory(Project, ProjectCollection, extra=0, form=ProjectCollectionForm)
"""Formset for the ProjectCollection model"""

ProjectRoleFormset = inlineformset_factory(Project, ProjectRole, extra = 1)
"""Formset for the ProjectRole model"""

# -------------------------------------------------------------------------------- #
from django.conf import settings
from django.db.models import get_model

from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Fieldset, Layout, ButtonHolder, Submit
from django_select2.fields import AutoModelSelect2Field,  AutoModelSelect2MultipleField


class SequenceForm(forms.ModelForm):
    """Sequence ModelForm for the Update and Create views."""

    resources = forms.CharField()

    class Meta:
        model = Sequence
        exclude = ['created', 'user', 'collection',]

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(SequenceForm, self).__init__(*args, **kwargs)
        self.fields['resources'].widget = forms.HiddenInput()
        self.helper = FormHelper()
        self.helper.form_id = 'sequence-form'
        self.form_error_title = 'Invalid form: please correct the errors below:'

    def clean_resources(self):
        resources = self.data.get('resources', None)
        return [int(k) for k in resources.split(',')]

    def clean(self):
        cd = super(SequenceForm, self).clean()
        resources = cd.get('resources')
        if not resources:
            raise forms.ValidationError("You did not select any resources.")
        return cd


class Select2AjaxChoices(AutoModelSelect2Field):

    def __init__(self, *args, **kwargs):
        self.search_fields = kwargs.pop('search_fields')
        super(Select2AjaxChoices, self).__init__(*args, **kwargs)
        self.widget.options.update({
            'width': '100%',
            'placeholder': u"Select %s" % self.queryset.model._meta.model_name
        })

    def security_check(self, request, *args, **kwargs):
        user = request.user
        if not user.is_authenticated():
            return False
        return True


class Select2MultipleAjaxChoices(AutoModelSelect2MultipleField):

    def __init__(self, *args, **kwargs):
        self.search_fields = kwargs.pop('search_fields')
        super(Select2MultipleAjaxChoices, self).__init__(*args, **kwargs)
        self.widget.options.update({
            'width': '100%',
            'placeholder': u"Select subset of %s" % self.queryset.model._meta.model_name
        })

    def security_check(self, request, *args, **kwargs):
        user = request.user
        if not user.is_authenticated():
            return False
        return True


TARGET_CHOICES = {
    ('S', 'Static'),
    ('D', 'Dynamic'),
}

class PredefinedAttributeForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(PredefinedAttributeForm, self).__init__(*args, **kwargs)
        predefined_attrs_simple = settings.PREDEFINED_ATTRIBUTES_SIMPLE
        predefined_attrs_models = settings.PREDEFINED_ATTRIBUTES_MODELS
        target_form_field = forms.ChoiceField(
                choices=TARGET_CHOICES,
                label = 'Target form',
                help_text = 'choose a target form for this attribute',
                required = False,
                initial = 'D',
            )
        required_form_field = forms.BooleanField(
            label = 'Required',
            required = False,
            initial = False,
        )
        layout = []
        # create form fields for simple attributes predefined in settings.py
        for pre_attr in predefined_attrs_simple.keys():
            layout.append(HTML('<div class="panel panel-default"><div class="panel-body">'),)
            self.fields[pre_attr] = forms.BooleanField(required=False, label=predefined_attrs_simple[pre_attr]['label'], help_text=predefined_attrs_simple[pre_attr]['help_text'],)
            layout.append(pre_attr)
            self.fields['target_%s' % pre_attr] = target_form_field
            layout.append('target_%s' % pre_attr)
            self.fields['required_%s' % pre_attr] = required_form_field
            layout.append('required_%s' % pre_attr)
            layout.append(HTML('</div></div>'))
        # create form fields for model-based attributes predefined in settings.py
        for pre_attr in predefined_attrs_models.keys():
            layout.append(HTML('<div class="panel panel-default"><div class="panel-body">'),)
            model = get_model(predefined_attrs_models[pre_attr]['app'], pre_attr)
            self.fields[pre_attr] = forms.BooleanField(
                required=False,
                label=predefined_attrs_models[pre_attr]['label']
            )
            layout.append(pre_attr)
            '''
            for _filter in predefined_attrs_models[pre_attr]['filters']:
                _choices = list(set(model.objects.values_list(_filter, _filter)))
                _choices.sort()
                #_choices = [('', 'Filter by %s' % _filter),('', '-------------------')] + _choices
                self.fields['filter_%s_%s' % (pre_attr, _filter)] = Select2MultipleChoiceField(
                    choices = _choices,
                    required = False,
                    label = '',
                )
                self.fields['filter_%s_%s' % (pre_attr, _filter)].widget.options.update({
                    'width': '100%',
                    'placeholder': 'Filter by selected %s' % _filter
                })
                layout.append('filter_%s_%s' % (pre_attr, _filter))
            '''
            self.fields['selected_%s' % pre_attr] = Select2MultipleAjaxChoices(
                queryset = model.objects,
                search_fields = predefined_attrs_models[pre_attr]['search_fields'],
                required = False,
                label = '',
            )
            layout.append('selected_%s' % pre_attr)
            self.fields['target_%s' % pre_attr] = target_form_field
            layout.append('target_%s' % pre_attr)
            self.fields['required_%s' % pre_attr] = required_form_field
            layout.append('required_%s' % pre_attr)
            layout.append(HTML('</div></div>'))
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(*layout)


TYPE_STR = 'S'
TYPE_INT = 'I'
TYPE_FLT = 'F'
TYPE_BOL = 'B'

TYPE_CHOICES = {
    (TYPE_BOL, 'Boolean'),
    (TYPE_FLT, 'Float'),
    (TYPE_INT, 'Integer'),
    (TYPE_STR, 'String'),
}

#NgModelFormMixin
class AttributeForm(forms.Form):

    at_name = forms.CharField(
        label = 'Name',
        max_length = 80,
        required = False,
    )
    at_type = forms.ChoiceField(
        choices=TYPE_CHOICES,
        label = 'Type',
        required = False,
        initial = 'S',
    )
    target = forms.ChoiceField(
        choices=TARGET_CHOICES,
        label = 'Target form',
        help_text = 'choose a target form for this attribute',
        required = False,
        initial = 'D',
    )
    values = forms.CharField(
        label = 'Values',
        max_length = 80,
        help_text = 'define possible values for this attribute (comma separated)',
        required = False,
    )
    initial = forms.CharField(
        label = 'Initial',
        max_length = 80,
        help_text = 'define initial value',
        required = False,
    )
    required = forms.BooleanField(
        label = 'Required',
        required = False,
        initial = False,
    )

    def __init__(self, *args, **kwargs):
        super(AttributeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML('<a style="color:inherit;" data-toggle="collapse" data-parent="#accordion" href="#custom"><h3>Define your custom attributes</h3></a>'),
            Fieldset(' ',
                     HTML('<div id="custom" class="panel-collapse collapse in">'),
                     'at_name',
                     'at_type',
                     'target',
                     'values',
                     'initial',
                     'required',
                     HTML(
                         '<input type="submit" name="submit" value="Add / Update" class="btn btn-success btn-sm" id="submit-save">'
                     ),
            ),
        )

    def clean(self):
        cd = super(AttributeForm, self).clean()
        at_name = cd.get('at_name')
        at_type = cd.get('at_type')
        values = cd.get('values')
        initial = cd.get('initial')

        test_numeric_types = {
            'F': float,
            'I': int
        }

        if not at_name and (values or initial):
            self._errors['at_name'] = self.error_class([u'You have forgotten about this field'])
            del cd['at_name']
            return cd

        if at_type == 'B' and values:
            self._errors['values'] = self.error_class([u'Boolean type has two default possible values: "True" and "False". You do not need to specify anything here.'])
            del cd['values']
            return cd

        if at_type == 'B' and initial and initial not in ('True', 'False'):
            self._errors['initial'] = self.error_class([u'Wrong value. Boolean type has two possible values: "True" and "False".'])
            del cd['initial']
            return cd

        if values:
            values_list = values.split(',')

            if len(values_list) < 2:
                self._errors['values'] = self.error_class([u'Parser error. Are these values really "comma-separated"?'])
                del cd['values']
                return cd

            if initial and not initial in values_list:
                self._errors['initial'] = self.error_class([u'Initial value must be one of defined possible values.'])
                del cd['initial']

            if at_type in test_numeric_types.keys():
                for v in values_list:
                    try:
                        test_numeric_types[at_type](v)
                    except ValueError as e:
                        self._errors['values'] = self.error_class([u'Value error: %s' % e])
                        del cd['values']
                        return cd

        else:
            if initial and at_type in test_numeric_types.keys():
                try:
                    test_numeric_types[at_type](initial)
                except ValueError as e:
                    self._errors['initial'] = self.error_class([u'Value error: %s' % e])
                    del cd['initial']
        return cd


class AttributeSetForm(forms.ModelForm):

    class Meta:
        model = AttributeSet
        exclude = ['owner',]

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(AttributeSetForm, self).__init__(*args, **kwargs)
        self.fields['custom_attrs'].widget = forms.HiddenInput()
        self.fields['predefined_attrs'].widget = forms.HiddenInput()
        self.fields['dynamic_attrs_order'].widget = forms.HiddenInput()
        self.fields['static_attrs_order'].widget = forms.HiddenInput()
        self.fields['project'].queryset = Project.objects.filter(
            cproject_roles__user__id=user.id, cproject_roles__name__in=ProjectRole.ROLE_EDIT
        )
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML('<h3><i class="icon-file"></i> Define your AttributeSet</h3>'),
            Fieldset(' ',
                'name',
                'project',
                'dynamic_attrs_order',
                'static_attrs_order',
                'custom_attrs',
                'predefined_attrs',
            ),
        )

class ReadOnlyFieldsMixin(object):
    readonly_fields =()
    def __init__(self, *args, **kwargs):
        super(ReadOnlyFieldsMixin, self).__init__(*args, **kwargs)
        for field in (field for name, field in self.fields.iteritems() if name in self.readonly_fields):
            field.widget.attrs['disabled'] = 'true'
            field.required = False
    def clean(self):
        cleaned_data = super(ReadOnlyFieldsMixin,self).clean()
        for field in self.readonly_fields:
           cleaned_data[field] = getattr(self.instance, field)
        return cleaned_data

from django.utils.datastructures import SortedDict
# create classification forms (dynamic and static) dynamically based on an attributeset object
class ClassificationForm(ReadOnlyFieldsMixin, forms.Form):

    def __init__(self, *args, **kwargs):
        fields_defs = kwargs.pop('fields_defs')
        attrs_order = kwargs.pop('attrs_order')
        readonly = kwargs.pop('readonly')
        inline = kwargs.pop('inline')
        self.base_fields = SortedDict(fields_defs)
        self.base_fields.keyOrder = attrs_order
        if readonly:
            self.readonly_fields = tuple(attrs_order)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.error_text_inline = False
        if inline:
            self.helper.template = 'crispy_forms/bootstrap/table_inline_formset.html'
        super(ClassificationForm, self).__init__(*args, **kwargs)

class ClassificationFilterForm(forms.Form):

    q = forms.CharField(label='Search', required=False, widget=forms.TextInput(attrs={'placeholder': 'Search'}))

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_show_errors = False
        self.helper.help_text_inline = False
        self.helper.form_tag = False
        # bootstrap3 inline form
        self.helper.form_class = 'form-inline'
        self.helper.field_template = 'crispy_forms/bootstrap3/layout/crispy_filter_field.html'
        self.helper.form_show_labels = False
        super(ClassificationFilterForm, self).__init__(*args, **kwargs)

