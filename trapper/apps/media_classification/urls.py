from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from trapper.apps.media_classification import views


urlpatterns = patterns('',
	# Home page of animal observation app
	url(r'^$', TemplateView.as_view(template_name='media_classification/index.html'), name='index'),

	# Display project list and the details about given project
	url(r'project/list/$', views.ProjectListView.as_view(), name='project_list'),
	url(r'project/detail/(?P<pk>\d+)/$', views.ProjectDetailView.as_view(), name='project_detail'),
        url(r'project/update/(?P<pk>\d+)/$', views.ProjectUpdateView.as_view(), name='project_update'),
	url(r'project/create/(?P<rp_pk>\d+)/$', views.ProjectCreateView.as_view(), name='project_create'),

	# Attributeset views
	url(r'attributeset/detail/(?P<pk>\d+)/$', views.AttributeSetDetailView.as_view(), name='attributeset_detail'),
	url(r'attributeset/list/$', views.AttributeSetListView.as_view(), name='attributeset_list'),

        url(r'attributeset/create/$', views.AttributeSetCreateView.as_view(), name='attributeset_create'),
        url(r'attributeset/update/(?P<pk>\d+)/$', views.AttributeSetUpdateView.as_view(), name='attributeset_update'),
	#url(r'attributeset/delete/(?P<pk>\d+)/$', views.AttributeSetDeleteView, name='attributeset_delete'),

	# Sequence views
	url(r'sequence/detail/(?P<sequence_pk>\d+)/$', views.SequenceDetailAjaxView.as_view(), name='sequence_detail'),
	url(r'sequence/list/(?P<collection_pk>\d+)/$', views.SequenceListAjaxView.as_view(), name='sequence_list'),
	url(r'sequence/create/(?P<collection_pk>\d+)/$', views.SequenceCreateUpdateAjaxView.as_view(), name='sequence_create'),
	url(r'sequence/update/(?P<sequence_pk>\d+)/$', views.SequenceCreateUpdateAjaxView.as_view(), name='sequence_update'),
	url(r'sequence/delete/(?P<sequence_pk>\d+)/$', views.SequenceDeleteAjaxView.as_view(), name='sequence_delete'),

	# Display the classification form and process (action) it
	url(r'classify/(?P<project_id>\d+)/(?P<resource_id>\d+)/(?P<collection_id>\d+)/$', views.classify_resource, name='classify'),
	url(r'classify/(?P<project_id>\d+)/(?P<resource_id>\d+)/(?P<collection_id>\d+)/(?P<user_id>\d+)/$', views.classify_resource, name='classify_user'),
	#url(r'classify/action/$', views.process_classify, name='classify_action'),

	# Displays the details about given classification
	url(r'classification/detail/(?P<pk>\d+)/$', views.ClassificationDetailView.as_view(), name='classification_detail'),
 	url(r'classification/list/(?P<pk>\d+)/$', views.ClassificationListView.as_view(), name='classification_list'),
 	url(r'classification/delete/(?P<pk>\d+)/$', views.ClassificationDeleteView.as_view(), name='classification_delete'),

	#url(r'crowd/list/$', views.cs_resource_list, name='cs_resource_list'),
)
