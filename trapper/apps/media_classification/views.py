from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views import generic
from django.http import HttpResponseRedirect, HttpResponseNotAllowed
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.template.loader import render_to_string

from braces.views import LoginRequiredMixin, UserPassesTestMixin, JSONResponseMixin, AjaxResponseMixin
from extra_views import InlineFormSet, CreateWithInlinesView, UpdateWithInlinesView, NamedFormsetsMixin

from trapper.apps.media_classification.models import Project, Classification, ClassificationDynamicFormRow, ClassificationUser, ClassificationUserDynamicFormRow, ProjectRole, ProjectCollection, AttributeSet, Sequence
from trapper.apps.research.models import Project as RProject
from trapper.apps.storage.models import Resource
from trapper.apps.media_classification.forms import ProjectForm, AttributeSetForm, SequenceForm
from .forms import AttributeForm, PredefinedAttributeForm, ClassificationForm

from functools import partial, wraps
import datetime


#---------------------------------------------------------------------------------------#
# ATTRIBUTESET VIEWS
#---------------------------------------------------------------------------------------#

class AttributeSetDetailView(generic.DetailView):
    """Detail view of a single AttributeSet model"""

    model = AttributeSet

class AttributeSetListView(generic.ListView):
    """List view of a AttributeSet model"""

    model = AttributeSet
    context_object_name = 'attributesets'

class AttributeSetEditMixin(generic.DetailView):

    template_name = 'media_classification/attributeset_form.html'
    model = AttributeSet
    pre_at_initial = None
    at_initial = None
    attrs_order = None
    object = None

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        atset_form = AttributeSetForm(instance=self.object, user=request.user)
        pre_at_form = PredefinedAttributeForm(initial = self.pre_at_initial)
        at_form = AttributeForm(initial = self.at_initial)
        context.update({'atset_form': atset_form, 'pre_at_form': pre_at_form, 'at_form': at_form, 'attrs_order': self.attrs_order})
        messages.warning(request,
                         'This is a place where you can define a set of attributes that will be a part of classification form.'
        )
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        atset_form_save = AttributeSetForm(request.POST, instance=self.object, user=request.user)
        at_form_save = AttributeForm(request.POST)
        pre_at_form_save = PredefinedAttributeForm(request.POST)

        if atset_form_save.is_valid() and at_form_save.is_valid() and pre_at_form_save.is_valid():
            instance = atset_form_save.save(commit=False)
            if not self.object:
                instance.owner = request.user
            # parse data from select2 multiplechoice inputs
            for at in pre_at_form_save.cleaned_data.keys():
                if at.startswith('selected_'):
                    pre_at_form_save.cleaned_data[at] = [k.pk for k in pre_at_form_save.cleaned_data[at]]
            instance.predefined_attrs = dict(pre_at_form_save.cleaned_data)
            cd = at_form_save.cleaned_data
            if cd['at_name']:
                instance.custom_attrs[cd.pop('at_name')] = cd
            else:
                messages.success(request, 'Your changes have been saved successfully.')
            instance.save()
            instance.update_attrs_order()
        else:
            context.update({'atset_form': atset_form_save, 'pre_at_form': pre_at_form_save, 'at_form': at_form_save,})
            return self.render_to_response(context)
        if not self.object:
            return redirect('media_classification:attributeset_update', instance.pk)
        return self.get(request, *args, **kwargs)


class  AttributeSetCreateView(LoginRequiredMixin, AttributeSetEditMixin):

    def get_object(self):
        self.object = None

class  AttributeSetUpdateView(LoginRequiredMixin, UserPassesTestMixin, AttributeSetEditMixin):

    def test_func(self, user):
        return self.model.can_edit

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.pre_at_initial = self.object.parse_hstore_values('predefined_attrs')
        return super(AttributeSetUpdateView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.POST.get('updateAttr'):
            at_name = request.POST['updateAttr']
            self.at_initial = self.object.parse_hstore_values('custom_attrs', at_name)[at_name]
            self.at_initial.update({'at_name': at_name})
            return self.get(request, *args, **kwargs)
        if request.POST.get('removeAttr'):
            del self.object.custom_attrs[request.POST['removeAttr']]
            self.object.save()
            return self.get(request, *args, **kwargs)
        if request.POST.get('clone'):
            newobj = self.object
            newobj.pk = None
            newobj.name = self.object.name + '_CLONED'
            newobj.save()
            return redirect('media_classification:attributeset_update', newobj.pk)
        return super(AttributeSetUpdateView, self).post(request, *args, **kwargs)


'''
class AttributeSetDeleteView(generic.DeleteView):
    """Delete view of the AttributeSet model"""

    model = AttributeSet
'''

#---------------------------------------------------------------------------------------#
# CLASSIFICATION PROJECT VIEWS
#---------------------------------------------------------------------------------------#

class ProjectListView(generic.ListView):
    """List view of the Project model"""

    model = Project
    context_object_name = 'items'
    template_name = 'media_classification/project_list.html'

    def get_queryset(self, *args, **kwargs):
        """Besides getting the queryset, determines the permissions for request.user.

        :return: a list of tuples, each containing the following: (:class:`.Project`, user_can_view, user_can_edit)
        :rtype: list
        """

        projects = super(ProjectListView, self).get_queryset(*args, **kwargs)
        user = self.request.user
        items = []
        for p in projects:
            roles = p.determine_roles(user) if user.is_authenticated() else []
            items.append((p, len(roles) > 0, ProjectRole.ROLE_PROJECT_ADMIN in roles))
        return items

class ProjectDetailView(LoginRequiredMixin, UserPassesTestMixin, generic.DetailView):
    """Detail view for the Project model"""

    model = Project

    def test_func(self, user):
        return self.model.can_detail

    def dispatch(self, *args, **kwargs):
        return super(ProjectDetailView, self).dispatch(*args, **kwargs)

class ProjectRoleInline(InlineFormSet):
    """Utility-class: ProjectRoles displayed as a InlineFormset"""

    model = ProjectRole
    extra = 2

class CollectionInline(InlineFormSet):
    """Utility-class: Collections displayed as a InlineFormset"""

    model = ProjectCollection
    extra = 2

class ProjectCreateView(CreateWithInlinesView, NamedFormsetsMixin):
    """Create view for the Project model"""

    model = Project
    form_class = ProjectForm
    template_name = 'media_classification/project_update.html'
    inlines = [ProjectRoleInline, CollectionInline]
    inlines_names = ['projectrole_formset', 'collection_formset']

    def get_initial(self, *args, **kwargs):
        initial = super(ProjectCreateView, self).get_initial(*args, **kwargs)
        initial['rp_pk'] = self.kwargs['rp_pk']
        #initial['collections'] = self.kwargs['rp_pk']
        return initial

    def forms_valid(self, form, inlines):
        """Saves the formsets and redirects to Project's detail page."""

        self.object = form.save(commit=False)
        rproject = get_object_or_404(RProject, pk=form.cleaned_data['rp_pk'])
        self.object.research_project=rproject
        self.object.save()
        projectrole_formset = inlines[0]
        projectrole_formset.save()
        return HttpResponseRedirect(self.object.get_absolute_url())

class ProjectUpdateView(UpdateWithInlinesView, NamedFormsetsMixin):
    """Update view for the Project model"""

    model = Project
    form_class = ProjectForm
    template_name = 'media_classification/project_update.html'
    inlines = [ProjectRoleInline, CollectionInline]
    inlines_names = ['projectrole_formset', 'collection_formset']

    def forms_valid(self, form, inlines):
        """Saves the formsets and redirects to Project's detail page."""

        self.object = form.save(commit=False)
        self.object.save()
        projectrole_formset, collection_formset = inlines
        projectrole_formset.save()
        # Save collection formset manually as it is defined using the intermediate model
        for pc in collection_formset:
            pc_obj = pc.save(commit=False)
            pc_obj.project_id = self.object.id
            pc_obj.collection_id = pc_obj.collection.id
            pc_obj.save()
        return HttpResponseRedirect(self.object.get_absolute_url())

#---------------------------------------------------------------------------------------#
# CLASSIFICATION VIEWS
#---------------------------------------------------------------------------------------#

from django.forms.models import formset_factory
from trapper.apps.common.tools import parse_hstore_field
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied

@login_required
def classify_resource(request, project_id, resource_id, collection_id, user_id=None):
    """Prepares the context for the classification table (i.e. atrributeset), and renders it on a template."""
    dynamic_class_form = None
    static_class_form = None

    try:
        project = Project.objects.get(id=project_id)
        resource = Resource.objects.get(id=resource_id)
        pcollection = ProjectCollection.objects.get(id=collection_id, project=project)
    except (Project.DoesNotExist, ProjectCollection.DoesNotExist, Resource.DoesNotExist) as e:
        raise e
    if not resource in pcollection.collection.collection.resources.all():
        raise Resource.DoesNotExist

    # test user permissions
    if not project.can_detail(request.user):
        raise PermissionDenied()
    user_is_admin = project.is_project_admin(request.user)

    resources = pcollection.get_resources_with_status()
    classification = Classification.objects.get_or_create(project=project, resource=resource, pcollection=pcollection)[0]

    # check if classification has been already approved; if yes and user is not an admin make sure the class form is readonly
    if user_id:
        class_user = User.objects.get(pk=user_id)
        classification_user = ClassificationUser.objects.get_or_create(classification=classification, user=class_user)[0]
        if user_is_admin:
            class_form_readonly = False
        elif class_user == request.user:
            class_form_readonly = False
        else:
            class_form_readonly = True
    else:
        class_user = None
        if classification.approved:
            classification_user = classification
            if user_is_admin:
                class_form_readonly = False
            else:
                class_form_readonly = True
        else:
            return redirect('media_classification:classify_user',
                            project_id,
                            resource_id,
                            collection_id,
                            request.user.id)

    try:
        attributeset = project.attributeset
    except AttributeSet.DoesNotExist:
        attributeset = None

    if attributeset:
        # http://stackoverflow.com/questions/622982/django-passing-custom-form-parameters-to-formset
        fields_defs = attributeset.prepare_class_form_fields_defs()
        if fields_defs['D']:
            dynamic_attrs_order = attributeset.get_dynamic_attrs_order()
            dynamic_class_form_initial = []
            for data in classification_user.data_dynamic_form_rows.values_list('data_dynamic_form_row', flat=True):
                dynamic_class_form_initial.append(
                    parse_hstore_field(data)
                )
            if dynamic_class_form_initial:
                extra=0
            else:
                extra=1
            ClassFormSet = formset_factory(
                wraps(ClassificationForm)(
                    partial(
                        ClassificationForm,
                        fields_defs=fields_defs['D'],
                        attrs_order=dynamic_attrs_order,
                        inline=True,
                        readonly=class_form_readonly,
                    )
                ),
                extra=extra,
            )
            dynamic_class_form = ClassFormSet(initial=dynamic_class_form_initial)

        if fields_defs['S']:
            static_attrs_order = attributeset.get_static_attrs_order()
            static_class_form_initial = None
            if classification_user.data_static_form:
                static_class_form_initial = parse_hstore_field(classification_user.data_static_form)
            static_class_form = ClassificationForm(
                fields_defs=fields_defs['S'],
                attrs_order=static_attrs_order,
                initial=static_class_form_initial,
                inline=False,
                readonly=class_form_readonly,
            )

        # POST - save classifications
        if request.method == 'POST' and not class_form_readonly:
            # process data POST'ed by dynamic form
            if dynamic_class_form:
                dynamic_class_form = ClassFormSet(request.POST)
                if dynamic_class_form.is_valid():
                    # bulk-delete of old rows
                    classification_user.data_dynamic_form_rows.all().delete()
                    for form in dynamic_class_form.forms: #cleaned_data:
                        # create new rows
                        if class_user:
                            row = ClassificationUserDynamicFormRow(
                                classification_user = classification_user,
                                data_dynamic_form_row = form.cleaned_data,
                            )
                        else:
                            row = ClassificationDynamicFormRow(
                                classification = classification_user,
                                data_dynamic_form_row = form.cleaned_data,
                            )
                        row.save()
            # process data POST'ed by static form
            if static_class_form:
                static_class_form = ClassificationForm(
                    request.POST,
                    fields_defs=fields_defs['S'],
                    attrs_order=static_attrs_order,
                    inline=False,
                    readonly=False,
                )
                if static_class_form.is_valid():
                    classification_user.data_static_form = static_class_form.cleaned_data
                    classification_user.save()

            if user_is_admin and request.POST.get('Approve'):
                classification.approved = True
                classification.approved_by = request.user
                classification.approved_date = datetime.datetime.now()
                if static_class_form:
                    classification.data_static_form = static_class_form.cleaned_data
                classification.save()
                if dynamic_class_form:
                    classification.data_dynamic_form_rows.all().delete()
                    for form in dynamic_class_form.forms: #cleaned_data:
                        # create new rows
                        row = ClassificationDynamicFormRow(
                            classification = classification,
                            data_dynamic_form_row = form.cleaned_data,
                        )
                        row.save()

    context = {'resource': resource, 'resources': resources, 'project': project, 'collection': pcollection, 'dynamic_class_form': dynamic_class_form, 'static_class_form': static_class_form, 'classification': classification, 'class_user': class_user, 'user_is_admin': user_is_admin,}
    return render(request, 'media_classification/classify_resource.html', context)


class ClassificationDetailView(generic.DetailView):
    """Detail view of the Classification object."""

    model = Classification
    template_name = 'media_classification/classification_detail.html'


from django.views.generic.detail import SingleObjectMixin
from .filters import ClassificationFilter
from django.db.models import Q
# Zapytac o to Kube
# from django_actions.views import ActionViewMixin
from .actions import test_action

# class ClassificationListView(UserPassesTestMixin, ActionViewMixin, SingleObjectMixin, generic.ListView):
class ClassificationListView(UserPassesTestMixin, SingleObjectMixin, generic.ListView):
    """List view of the Classification object."""

    paginate_by = 5
    template_name = 'media_classification/classification_list.html'
    list_model_filter = ClassificationFilter
    search_request_str = 'q'
    search_fields = ('resource__name', 'data_static_form', 'data_dynamic_form_rows__data_dynamic_form_row')
    raise_exception = True
    actions = [test_action,]

    def test_func(self, user):
        project = Project.objects.get(pk=self.kwargs['pk'])
        if not project.is_project_admin(user):
            return False
        else:
            return True

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Project.objects.all())
        return super(ClassificationListView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ClassificationListView, self).get_context_data(**kwargs)
        filters = self.list_model_filter(self.request.GET, project=self.object)
        context.update({
            'project': self.object,
            'filtering_form': filters.form,
            'qs_all_len': self.qs_all_len,
            'qs_filtered_len': self.qs_filtered_len,
        })
        return context

    def get_queryset(self):
        qs = self.object.classification_set.all().order_by('resource__date_recorded')
        self.qs_all_len = len(qs)
        qs = self.list_model_filter(self.request.GET, queryset=qs, project=self.object).qs
        qs = self.search_data(qs).distinct()
        self.qs_filtered_len = len(qs)
        return qs

    def search_data(self, queryset, *args, **kwargs):
        search = self.request.GET.get(self.search_request_str)
        if search:
            search_words = search.split()
            filters = None
            for f in self.search_fields:
                for word in search_words:
                    q = Q(**{f + '__icontains': word})
                    filters = filters | q if filters else q
            if filters:
                queryset = queryset.filter(filters)
        return queryset


from django.core.urlresolvers import reverse
class ClassificationDeleteView(UserPassesTestMixin, generic.DeleteView):
    """Detail view of the Classification object."""

    model = Classification

    def test_func(self, user):
        project = Classification.objects.get(pk=self.kwargs['pk']).project
        if not project.is_project_admin(user):
            return False
        else:
            return True

    def get_success_url(self):
        return reverse('media_classification:classification_list', args=[self.object.project.pk,])

#---------------------------------------------------------------------------------------#
# SEQUENCES VIEWS
#---------------------------------------------------------------------------------------#

# TODO: make test_functions more generic, there is a lot of code repetition at the moment
from trapper.apps.common.views import AjaxModelFormMixin

class SequenceCreateUpdateAjaxView(AjaxModelFormMixin):
    """Ajax based create/update view for the Sequence model"""

    model = Sequence
    form = SequenceForm
    form_user_kwargs = True
    save_m2m = True
    object_kwarg_name = 'sequence_pk'

    def test_func(self, user):
        if self.kwargs.has_key(self.object_kwarg_name):
            pcollection = Sequence.objects.get(pk=self.kwargs[self.object_kwarg_name]).collection
        else:
            pcollection = ProjectCollection.objects.get(pk=self.kwargs['collection_pk'])
        user_roles = pcollection.project.determine_roles(user)
        if not user_roles or 'C' in  user_roles:
            return False
        elif 'E' in user_roles and not pcollection.sequencing_experts:
            return False
        else:
            return True

    def update_form_fields(self, obj, request, form):
        if not obj.pk:
            obj.user = request.user
            obj.collection = ProjectCollection.objects.get(pk=self.kwargs['collection_pk'])
        resource_objects = Resource.objects.filter(pk__in=form.cleaned_data.get('resources'))
        if not set(resource_objects).issubset(obj.collection.collection.collection.resources.all()):
            form.errors['__all__'] = form.error_class(["There is something wrong with your selection."])
        return obj, form


class SequenceListAjaxView(UserPassesTestMixin, LoginRequiredMixin, JSONResponseMixin, AjaxResponseMixin, generic.View):

    raise_exception = True

    def test_func(self, user):
        pcollection = ProjectCollection.objects.get(pk=self.kwargs['collection_pk'])
        user_roles = pcollection.project.determine_roles(user)
        if not user_roles:
            return False
        else:
            return True

    def get_ajax(self, request, *args, **kwargs):
        """collection = ProjectCollection"""
        collection_pk = self.kwargs['collection_pk']
        objects = Sequence.objects.filter(collection__pk=collection_pk)
        data = {}
        for k in objects:
            data[k.pk] = list(k.resources.values_list('pk', flat=True))
        return self.render_json_response(data)


class SequenceDetailAjaxView(UserPassesTestMixin, LoginRequiredMixin, JSONResponseMixin, AjaxResponseMixin, generic.View):
    """Ajax based detailed view for the Sequence model"""

    raise_exception = True
    can_edit= False

    def test_func(self, user):
        pcollection = Sequence.objects.get(pk=self.kwargs['sequence_pk']).collection
        user_roles = pcollection.project.determine_roles(user)
        if not user_roles:
            return False
        elif 'C' in user_roles or ('E' in user_roles and not pcollection.sequencing_experts):
            self.can_edit = False
            return True
        else:
            self.can_edit = True
            return True

    def get_ajax(self, request, *args, **kwargs):
        sequence_pk = self.kwargs['sequence_pk']
        sequence = Sequence.objects.get(pk=sequence_pk)
        resources = list(sequence.resources.values_list('pk', flat=True))
        html = render_to_string('media_classification/sequence_detail.html', {'sequence': sequence, 'resources_len': len(resources)})
        return self.render_json_response({'html': html, 'resources': resources, 'can_edit': self.can_edit})


class SequenceDeleteAjaxView(UserPassesTestMixin, LoginRequiredMixin, JSONResponseMixin, AjaxResponseMixin, generic.View):
    """Ajax based delete view for the Sequence model"""

    raise_exception = True

    def test_func(self, user):
        pcollection = Sequence.objects.get(pk=self.kwargs['sequence_pk']).collection
        user_roles = pcollection.project.determine_roles(user)
        if not user_roles or 'C' in  user_roles:
            return False
        elif 'E' in user_roles and not pcollection.sequencing_experts:
            return False
        else:
            return True

    def get_ajax(self, request, *args, **kwargs):
        sequence_pk = self.kwargs['sequence_pk']
        sequence = Sequence.objects.get(pk=sequence_pk)
        sequence.delete()
        return self.render_json_response({'msg': 'Sequence successfully deleted.',})

