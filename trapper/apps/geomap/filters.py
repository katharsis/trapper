import django_filters
from trapper.apps.geomap.models import Location
from trapper.apps.storage.models import Resource
from trapper.apps.geomap.forms import LocationFilterForm


#resource_types = [(k) for k in Resource.TYPE_CHOICES]
#resource_types.sort()
#RESOURCE_TYPE_FILTER_CHOICES = tuple([('','-- All --')]+resource_types)
#resource_status = [(k) for k in Resource.STATUS_CHOICES]
#RESOURCE_STATUS_FILTER_CHOICES = tuple([('','-- All --')]+resource_status)


class LocationFilter(django_filters.FilterSet):
    #resource_type = django_filters.ChoiceFilter(choices=RESOURCE_TYPE_FILTER_CHOICES, label='Type')
    #status = django_filters.ChoiceFilter(choices=RESOURCE_STATUS_FILTER_CHOICES)

    class Meta:
        model = Location
        #form = LocationFilterForm
        #fields = ['is_public', 'resource_type', 'status']
        fields = ['is_public', 'resource__resource_type', 'resource__status']

