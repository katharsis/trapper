from django import forms

from trapper.apps.geomap.models import Location
from trapper.apps.storage.models import Resource
import trapper.tools.gpxpy as gpxpy
from django.contrib.gis.geos import Point
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Field


resource_types = [(k) for k in Resource.TYPE_CHOICES]
resource_types.sort()
RESOURCE_TYPE_CHOICES = tuple([('','-- All --')]+resource_types)
resource_status = [(k) for k in Resource.STATUS_CHOICES]
RESOURCE_STATUS_CHOICES = tuple([('','-- All --')]+resource_status)
IS_PUBLIC_CHOICES = (
    ('', '-- All --'),
    (True, 'True'),
    (False, 'False')
)

class LocationFilterForm(forms.ModelForm):

    resource__resource_type = forms.ChoiceField(choices=RESOURCE_TYPE_CHOICES, label='Type', required=False)
    resource__status = forms.ChoiceField(choices=RESOURCE_STATUS_CHOICES, required=False)
    is_public = forms.ChoiceField(choices=IS_PUBLIC_CHOICES, required=False)

    class Meta:
        model = Location
        exclude = ['owner',]

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_id = 'filter-locations-form'
        self.helper.error_text_inline = True
        self.helper.form_show_errors = False
        self.helper.help_text_inline = False
        self.helper.form_show_labels = True
        self.helper.layout = Layout(
            Fieldset(
                'Location properties',
                'is_public',
            ),
            Fieldset(
                'Resources properties',
                'resource__resource_type',
                'resource__status',
            ),
        )
        super(LocationFilterForm, self).__init__(*args, **kwargs)


class LocationUploadForm(forms.Form):
    """Upload form for the locations file."""

    gpx_file=forms.FileField()
    is_public = forms.BooleanField(initial=False, required=False)

    def add_locations(self, user):
        """Parses the .gpx file and looks for the locations which aren't available in the system yet.

        :param user: user adding the locations (made the Location's owner)
        :type user: :py:class:`django.contrib.auth.models.User`
        """

        gpx_file = self.cleaned_data['gpx_file']
        gpx = gpxpy.parse(gpx_file)
        no_created = 0
        for waypoint in gpx.waypoints:
            if Location.objects.filter(location_id=waypoint.name).count() == 0:
                no_created += 1
                Location.objects.create(location_id=waypoint.name,
                                        coordinates=Point(waypoint.longitude, waypoint.latitude),
                                        owner=user,
                                        is_public=self.cleaned_data['is_public']
                )
        return no_created
