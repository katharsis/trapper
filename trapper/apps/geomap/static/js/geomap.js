var parseQueryString = function() {
    var query = (location.search || '?').substr(1),
    map   = {};
    query.replace(/([^&=]+)=?([^&]*)(?:&+|$)/g, function(match, key, value) {
        (map[key] = value)
    });
    return map;
}

var updateURLQueryParam = function(param, newval, url) {
    var urlParts = url.split('?') 
    if ( !urlParts[1] ) {
	return [urlParts[0], '?', param, '=', newval].join('');  
    };
    var regex = RegExp(param + '=([^&]*)').exec(urlParts[1]);
    if ( regex ) {
	return url.replace([param, '=', regex[1]].join(''), [param, '=', newval].join(''));
    } else {
	return [url,'&',param,'=',newval].join('');
    };
};


var FilterLocationsControl = L.Control.extend({

    //options: {
     //   position: 'topright'
    //},

    onAdd: function (map) {
        var container = L.DomUtil.create('div', 'leaflet-control-filter-locations storage-control'),
        edit = L.DomUtil.create('a', '', container);
        edit.href = '#';
        edit.title = L._("Filter locations");

        L.DomEvent
            .addListener(edit, 'click', L.DomEvent.stop)
            .addListener(edit, 'click', this.openSearchUI, this);
        return container;
    },

    openSearchUI: function (options) {
	var self = this;
	this.buildFilterLocationsContainer(options);
	L.Storage.fire("ui:start", {data: {html:this.FilterLocationsContainer}});
    },

    buildFilterLocationsContainer: function(options) {
	var container = L.DomUtil.create('div', 'filter-locations'),
            title = L.DomUtil.create('h4', '', container),
            searchInput = L.DomUtil.create('input', '', container),
	    filterform = L.DomUtil.create('div', 'filter-locations-form-div', container),
            searchButton = L.DomUtil.create('input', '', container),
            map = this, options;

	title.innerHTML = L._('Filter locations');
	searchButton.type = "button";
	searchButton.value = L._('Search');
	searchButton.className = "button";
	searchInput.type = "text";
	searchInput.placeholder = L._('Search for keywords');
	if (queryFilters['q']) {
	    searchInput.value = queryFilters['q'];
	}
	$.ajax({
	    url: '/geomap/location/filterform',
	    data: queryFilters,
	    success: function (json) {
                if (json) {
                    filterform.innerHTML = json.location_form_html;
                };
	    }
	});
    
	var refreshRemoteLayers = function () {
	    var q = searchInput.value;
	    queryFilters['q'] = q;
	    // get filter-locations-form and serialize it
	    var filterform_serialized = $('.filter-locations-form-div form').serializeArray();
            var layers = this._map.datalayers_index;
	    for ( var i = 0; i < layers.length; i++ ) {
		if (layers[i].options.trapperLayer) {
		    var remoteURL = layers[i].options.remoteData.url;
		    // search for keywords
		    var remoteURL = updateURLQueryParam('q', q, remoteURL);
		    // add filter parameters
		    for ( var j = 0; j < filterform_serialized.length; j++ ) {
			var remoteURL = updateURLQueryParam(filterform_serialized[j].name, filterform_serialized[j].value, remoteURL);
			queryFilters[filterform_serialized[j].name] = filterform_serialized[j].value;
		    }
		    layers[i].options.remoteData.url = remoteURL;
		    layers[i].fetchRemoteData();
		}
	    }
	    for (var property in queryFilters) {
		if (queryFilters.hasOwnProperty(property)) {
		    queryFiltersResource[property.split('__')[1]] = queryFilters[property] 
		}
	    };	
	    L.S.fire('ui:alert', {content: L._('Filtering done! ;) '+ remoteURL), level: 'info'});
	    };

	L.DomEvent
            .addListener(searchButton, 'click', L.DomEvent.stop)
            .addListener(searchButton, 'click', refreshRemoteLayers, this);

	this.FilterLocationsContainer = container;
    },     
});


$(document).ready(function() {
    MAP.addControl(new FilterLocationsControl());
    
    if (!window.object) {
	MAP.datalayers_index[0]._delete();
	MAP.disableEdit();
	window.onbeforeunload = null;
    }

    window.queryFilters = parseQueryString();
    window.queryFiltersResource = {};

    for (var property in queryFilters) {
	if (queryFilters.hasOwnProperty(property)) {
	    queryFiltersResource[property.split('__')[1]] = queryFilters[property] 
	}
    };

    var onFeatureClick = function(e){
	$.ajax({                                                                                     
            url: '/geomap/location/resources/',
            async: true,
	    data: $.extend({'location_pk': e.target.properties.pk}, queryFiltersResource),
            dataType: 'html',
            success: function (data) {
		L.Storage.fire("ui:start", {data: {html: data}})
            }
        });
    } 

    var addTrapperLayer = function(layerName, options) {	
	var layer = MAP._createDataLayer({name: L._(layerName)});
	$.extend(layer.options, options);
	layer.resetLayer();
	layer.fetchRemoteData();
	return layer;
    }

    var updateLayerEvents = function(layer) {	
	setTimeout(function(){
	    layer.eachLayer(function(f){
		f.on("click", onFeatureClick);
		f.view = function(){return;}
	    })
	},1000);
    };

    var _fetchRemoteData = L.Storage.DataLayer.prototype.fetchRemoteData
    L.Storage.DataLayer.prototype.fetchRemoteData = function() {
	if(this.options.trapperLayer) {
	    updateLayerEvents(this);
	};
	_fetchRemoteData.apply(this);
    }

    var optionsDefault = {
	color: "Brown",
	displayOnLoad: true,
	fillColor: "Brown",
	iconClass: "Drop",
	markercluster: true,
	trapperLayer: true,
    }

    // !window.object == map_new
    if (!window.object) {
	var colors = ['Maroon','Orange','Olive','Teal','Navy'].reverse()
	if (queryFilters['collections']) {
	    // ajax call for collections
	    $.ajax({                                                                                     
		url: '/geomap/location/collections/',
		async: true,
		data: queryFilters,
		dataType: 'json',
		success: function (data) {
		    delete queryFilters['collections']
		    for ( var i = 0; i < data.length; i++ ) {
			queryFilters['collection_pk'] = data[i]['pk']
			var options = {
			    color: colors.pop(),
			    remoteData: {
				dynamic: false,
				format: "geojson",
				url: "/geomap/location/geojson?"+ $.param(queryFilters),
			    },
			};
			delete queryFilters['collection_pk'];
			var layer = addTrapperLayer(data[i]['name'], $.extend(optionsDefault, options));
			};
		}
            });
 	} else {
	    var options = {
		remoteData: {
		    dynamic: false,
		    format: "geojson",
		    url: "/geomap/location/geojson?"+ $.param(queryFilters),
		},
	    };
	    var layer = addTrapperLayer('Locations', $.extend(optionsDefault, options));
	    setTimeout(function() {layer.zoomTo()}, 500);
	}
    };
    L.S.fire('ui:alert', {content: L._('Click on location to see linked resources!'), level: 'info'});

});

