from django.contrib import messages
from django.views import generic
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.shortcuts import render_to_response
from django.template import RequestContext

from braces.views import LoginRequiredMixin, JSONResponseMixin, AjaxResponseMixin
from leaflet_storage.views import MapView, MapNew
from crispy_forms.utils import render_crispy_form
from crispy_forms.layout import Layout, Fieldset

from trapper.apps.geomap.models import Location
from trapper.apps.storage.models import Resource, Collection
from trapper.apps.geomap.forms import LocationUploadForm, LocationFilterForm
from trapper.apps.storage.filters import ResourceFilter

class LocationDetailView(generic.DetailView):
    """Displays given location on the map.
    """
    model = Location
    context_object_name = 'location'
    template_name = 'geomap/location_detail.html'

class LocationListView(generic.ListView):
    """Displays list of locations.
    """
    model = Location
    context_object_name = 'locations'
    template_name = 'geomap/location_list.html'

class LocationUploadView(LoginRequiredMixin, generic.FormView):
    """Uploads location data from the gpx file.
    """

    template_name = 'geomap/location_upload.html'
    form_class = LocationUploadForm
    success_url = reverse_lazy('geomap:location_list')

    def form_valid(self, form):
        """Attaches a message once the form was validated."""

        new_locs = form.add_locations(self.request.user)

        if new_locs == 0:
            messages.warning(self.request, "<strong>Upload Info:</strong> No new Locations found!")
        elif new_locs == 1:
            messages.success(self.request, "<strong>Upload Info:</strong> %d new Location was added!" % (new_locs,))
        elif new_locs > 1:
            messages.success(self.request, "<strong>Upload Info:</strong> %d new Locations were added!" % (new_locs,))

        return super(LocationUploadView, self).form_valid(form)

from djgeojson.views import GeoJSONLayerView
from .filters import LocationFilter


class TrapperMapLayerView(LoginRequiredMixin, GeoJSONLayerView):
    # Options
    precision = 5   # float
    geometry_field = 'coordinates'
    properties = ['pk', 'location_id', 'owner', 'is_public',]
    model = Location
    search_fields = ('owner__username', 'location_id', 'resource__name')


    def get_queryset(self, *args, **kwargs):
        qs = super(TrapperMapLayerView, self).get_queryset(*args, **kwargs)
        # TODO: use permissions; in general user can map either his/her own resources (owner or manager) or collections
        # so there are only 2 cases: a) filter for user reosurces b) check if user really have perms for given collection
        # user can view given location when 1) is owner or manager 2) has perms to view at least 1 resource linked to this location
        collection_pk = self.request.GET.get('collection_pk')
        if collection_pk:
            qs = qs.filter(resource__collection__pk=collection_pk).distinct()
        filters = LocationFilter(self.request.GET, queryset=qs)
        qs = filters.qs.distinct()
        search = self.request.GET.get('q')
        if search:
            search_words = search.split()
            filters = None
            for f in self.search_fields:
                for word in search_words:
                    q = Q(**{f + '__icontains': word})
                    filters = filters | q if filters else q
            if filters:
                qs = qs.filter(filters)
        return qs


class LocationFilterFormView(JSONResponseMixin, AjaxResponseMixin, generic.View):

    def get_ajax(self, request, *args, **kwargs):
        location_form = render_crispy_form(LocationFilterForm(request.GET))
        return self.render_json_response({'location_form_html': location_form,})


class LocationResourcesView(generic.View):

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            location_pk = request.GET.get('location_pk')
            if location_pk:
                location = Location.objects.get(pk=location_pk)
                #TODO: user permissions
                qs = Resource.objects.filter(location=location)
                filtered = ResourceFilter(request.GET, queryset=qs)
                template = 'geomap/location_detail_map.html'
                data = {
                    'resources': filtered.qs,
                    'location': location,
                }
                return render_to_response(template, data, context_instance = RequestContext(request))


# get names of requested collections
class LocationCollectionAjaxView(JSONResponseMixin, AjaxResponseMixin, generic.View):

    def get_ajax(self, request, *args, **kwargs):
        qs = Collection.objects.filter(pk__in=request.GET['collections'].split(',')).values('pk', 'name').order_by('name')
        return self.render_json_response(list(qs))


from leaflet_storage.models import Map
class MapListView(generic.ListView):
    model = Map
    context_object_name = 'maps'
    template_name = 'geomap/map_list.html'

