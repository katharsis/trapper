from django.conf.urls import patterns, url
from django.views.generic import DetailView, UpdateView, ListView, CreateView, TemplateView, DeleteView

from trapper.apps.geomap import views
from.models import Location

urlpatterns = patterns('',
	# Index view:
	url(r'^$', TemplateView.as_view(template_name='geomap/index.html'), name='index'),
	url(r'location/detail/(?P<pk>\d+)/$', views.LocationDetailView.as_view(), name='location_detail'),
	url(r'location/list/$', views.LocationListView.as_view(), name='location_list'),
	url(r'location/upload/$', views.LocationUploadView.as_view(), name='location_upload'),
        url(r'location/geojson$', views.TrapperMapLayerView.as_view(model=Location), name='locations'),
        url(r'location/filterform$', views.LocationFilterFormView.as_view(), name="locations_filterform"),
        url(r'location/resources/$', views.LocationResourcesView.as_view(), name="location_resources"),
        url(r'location/collections/$', views.LocationCollectionAjaxView.as_view(), name="location_collections"),

        url(r'map/list/$', views.MapListView.as_view(), name="map_list"),
)
