from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from trapper.apps.research import views


urlpatterns = patterns('',
	# Home page of research project module
	url(r'^$', TemplateView.as_view(template_name='research/index.html'), name='index'),

	# Display project list and the details about given project
	url(r'project/list/$', views.ProjectListView.as_view(), name='project_list'),
	url(r'project/detail/(?P<pk>\d+)/$', views.ProjectDetailView.as_view(), name='project_detail'),
	url(r'project/create/$', views.ProjectCreateView.as_view(), name='project_create'),
	url(r'project/update/(?P<pk>\d+)/$', views.ProjectUpdateView.as_view(), name='project_update'),
	url(r'project/delete/(?P<pk>\d+)/$', views.ProjectDeleteView.as_view(), name='project_delete'),
	url(r'project/collection/create/$', views.ProjectCollectionCreateView.as_view(), name='project_collection_create'),
)
