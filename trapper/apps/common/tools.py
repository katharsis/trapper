import json

# some helper functions to decode structured data strored by django-hstore fields
def json_loads_helper(obj):
    try:
        return json.loads(obj)
    except (TypeError, ValueError):
        return obj

def parse_hstore_field(data):
    return dict(map(lambda (k,v): (k, json_loads_helper(v)), data.iteritems()))

