from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from django.utils.safestring import mark_safe

from trapper.apps.extra_tables.models import Species, SpeciesImportExport


class SpeciesAdmin(ImportExportModelAdmin):

    list_display = ['english_name', 'latin_name', 'family', 'genus', 'eol_link']
    list_filter = ['family', 'genus']
    search_fields = ['english_name', 'latin_name', 'family', 'genus']
    resource_class = SpeciesImportExport

    def eol_link(self, instance):
        return mark_safe(u'<a href="http://eol.org/search?q=%s&search=Go" target="_blank">Go</a>' % instance.latin_name)
    eol_link.short_description = 'EOL link'


admin.site.register(Species, SpeciesAdmin)


