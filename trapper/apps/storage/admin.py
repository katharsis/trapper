from django.contrib import admin
from trapper.apps.storage.models import Resource, Collection, CollectionUploadJob, ResourceUserObjectPermission, ResourceGroupObjectPermission, CollectionUserObjectPermission, CollectionGroupObjectPermission

from trapper.apps.storage.tasks import process_collection_upload

class ResourceInline(admin.StackedInline):
    model = Resource
    extra = 0

class CollectionAdmin(admin.ModelAdmin):
    model = Collection

def run_collection_upload_task(request, queryset):
    for job in queryset:
        process_collection_upload(job.pk)

run_collection_upload_task.short_description = "Retry the uploading task for selected jobs"

class CollectionUploadJobAdmin(admin.ModelAdmin):
    model = CollectionUploadJob
    actions=[run_collection_upload_task,]

admin.site.register(Resource)
admin.site.register(CollectionUploadJob, CollectionUploadJobAdmin)
admin.site.register(Collection, CollectionAdmin)
admin.site.register(ResourceUserObjectPermission)
admin.site.register(ResourceGroupObjectPermission)
admin.site.register(CollectionUserObjectPermission)
admin.site.register(CollectionGroupObjectPermission)

