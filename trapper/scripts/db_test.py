# This is the script which initializes the database with few instances of data for the basic models
# This script is also used by the test suite, alter it with caution.
#
# Apply this script using the following:
#     ./env/bin/python manage.py shell_plus < db_test.py

from django.contrib.auth.models import User, Group
from django.contrib.gis.geos import Point
from trapper.apps.storage.models import Resource, Collection
from trapper.apps.research.models import Project as RProject, ProjectRole as RProjectRole, ProjectCollection as RProjectCollection
from trapper.apps.media_classification.models import AttributeSet, Project, ProjectRole, ProjectCollection
from trapper.apps.geomap.models import Location
from trapper.apps.extra_tables.models import SpeciesImportExport
import tablib

# Users
u0 = User.objects.get(username='alice')

g0 = Group.objects.get(name='Staff')
u1 = User.objects.create_user('staff1','staff1@trapper.pl','staff1')
u1.groups.add(g0)

g1 = Group.objects.get(name='Expert')
u2 = User.objects.create_user('expert1','expert1@trapper.pl','expert1')
u2.groups.add(g1)

u3 = User.objects.create_user('user1','user1@foo.com','user1')
u4 = User.objects.create_user('user2','user2@foo.com','user2')

# Import Species data from species.csv
csv = 'test_data/species_small.csv'
d = tablib.Dataset()
d.csv = open(csv,'rb').read()
sie = SpeciesImportExport()
sie.import_data(d)

r_data = (
	("video_mp4",  u1, u2, 'video_1.mp4',),
	("video_ogv",  u2, u2, 'video_1.ogv',),
	("audio_mp3",  u3, u4, 'audio_1.mp3',),
	("audio_ogg",  u1, u3, 'audio_2.wav',),
	("audio_wav",  u3, u3, 'audio_3.ogg',),
	("image_jpg",  u3, u3, 'image_1.jpg',),
	("video_webm", u2, u2, 'video_1.webm',),
)

r_filepath = "test_data/media_samples/"

from django.core.files import File

for name, owner, uploader, filename in r_data:
	r = Resource(name=name, owner=owner, uploader=uploader)
	with open(r_filepath + filename,'rb') as r_file:
		r.file.save(filename, File(r_file), save=False)
	r.update_metadata(commit=True)

r1, r2, r3, r4, r5, r6 = Resource.objects.filter(pk__in=xrange(1,7))

# Collection
c1 = Collection.objects.create(name="Video2013", owner=u3)
c1.resources.add(r1, r2, r3)

c2 = Collection.objects.create(name="Audio2013", owner=u4)
c2.resources.add(r4, r5)

c3 = Collection.objects.create(name="Image2013", owner=u4)
c3.resources.add(r6)

rp1 = RProject.objects.create(name="ResearchProject1")
rpr0 = RProjectRole.objects.create(name=RProjectRole.ROLE_PROJECT_ADMIN, user=u0, project=rp1)
rpr2 = RProjectRole.objects.create(name=RProjectRole.ROLE_EXPERT, user=u4, project=rp1)

rpc1 = RProjectCollection.objects.create(project=rp1, collection=c1)
rpc2 = RProjectCollection.objects.create(project=rp1, collection=c2)
rpc3 = RProjectCollection.objects.create(project=rp1, collection=c3)

# Classification Project
p1 = Project.objects.create(name="ClassificationProject1", research_project=rp1)
p2 = Project.objects.create(name="ClassificationProject2", research_project=rp1)

# AttributeSet
predefined_attrs = {
        u'annotations': u'true',
        u'comments': u'true',
        u'required_annotations': u'false',
        u'required_comments': u'false',
        u'required_species': u'false',
        u'selected_species': u'[1, 2, 3, 4]',
        u'species': u'true',
        u'target_annotations': u'D',
        u'target_comments': u'D',
        u'target_species': u'D'
}
custom_attrs = {
        u'Age': u'{"initial": "", "target": "D", "required": true, "values": "Juvenile,Adult", "at_type": "S"}',
        u'EMPTY': u'{"initial": "False", "target": "S", "required": false, "values": "True,False", "at_type": "S"}',
        u'FTYPE': u'{"initial": "3", "target": "S", "required": false, "values": "1,2,3,4,5,6", "at_type": "S"}',
        u'Number': u'{"initial": "1", "target": "D", "required": false, "values": "", "at_type": "I"}',
        u'Quality': u'{"initial": "Good", "target": "S", "required": false, "values": "Terrible,Bad,Good,Excellent", "at_type": "S"}',
        u'Sex': u'{"initial": "", "target": "D", "required": true, "values": "Female,Male", "at_type": "S"}'
}
dynamic_attrs_order = u'annotations,comments,species,Number,Age,Sex'
static_attrs_order = u'FTYPE,Quality,EMPTY'

attset = AttributeSet.objects.create(name="test_attset", predefined_attrs=predefined_attrs, custom_attrs=custom_attrs, dynamic_attrs_order=dynamic_attrs_order, static_attrs_order=static_attrs_order, owner=u0, project=p1)

# Classification Project Collections
pc1 = ProjectCollection.objects.create(project=p1, collection=rpc1, active=True)
pc2 = ProjectCollection.objects.create(project=p1, collection=rpc2, active=True)
pc3 = ProjectCollection.objects.create(project=p1, collection=rpc3, active=False)

pr0 = ProjectRole.objects.create(name=ProjectRole.ROLE_PROJECT_ADMIN, user=u0, project=p1)
pr2 = ProjectRole.objects.create(name=ProjectRole.ROLE_EXPERT, user=u4, project=p1)

## Locations
loc1 = Location.objects.create(coordinates=Point(23.8607, 52.7015), owner=u1, location_id="ID_01")
loc2 = Location.objects.create(coordinates=Point(23.1510, 53.1367), owner=u2, location_id="ID_02")
loc3 = Location.objects.create(coordinates=Point(22.3027, 54.3076), owner=u1, location_id="ID_03")
