# Django settings for trapper project.

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (  # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS
import os

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

# Uncomment this line if your postgis is 2.1 or newer (input the correct version in that case)
POSTGIS_VERSION = (2, 1, 0)

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'trapper_db',
        'USER': 'trapper',
        'PASSWORD': 'trapper',
        'HOST': '',
        'PORT': '',
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Warsaw'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
#STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static').replace('\\','/')
STATIC_ROOT = ''
# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files

STATICFILES_DIRS = (  # Put strings here, like "/home/html/static" or "C:/www/django/static".
                      # Always use forward slashes, even on Windows.
                      # Don't forget to use absolute paths, not relative paths.
                      os.path.join(PROJECT_ROOT, 'static').replace('\\', '/'),
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',  # this is default
    'guardian.backends.ObjectPermissionBackend',
)

AUTH_PROFILE_MODULE = 'accounts.UserProfile'

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
'django.contrib.staticfiles.finders.AppDirectoriesFinder',
'django.contrib.staticfiles.finders.FileSystemFinder',
#	'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '!*!hketnrwg8txwk)u2v8w(n1)e@bt$mo-5)oxgjn___uk%zua'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
'django.template.loaders.filesystem.Loader',
'django.template.loaders.app_directories.Loader',
#	 'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
)

MIDDLEWARE_CLASSES = (
'django.middleware.common.CommonMiddleware',
'django.contrib.sessions.middleware.SessionMiddleware',
'django.middleware.csrf.CsrfViewMiddleware',
'django.contrib.auth.middleware.AuthenticationMiddleware',
'django.contrib.messages.middleware.MessageMiddleware',  # Uncomment the next line for simple clickjacking protection:
# 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'trapper.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'trapper.wsgi.application'

TEMPLATE_DIRS = (  # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
                   # Always use forward slashes, even on Windows.
                   # Don't forget to use absolute paths, not relative paths.
                   os.path.join(PROJECT_ROOT, 'templates').replace('\\', '/'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'django.contrib.comments',

    'trapper.apps.media_classification',
    'trapper.apps.storage',
    'trapper.apps.accounts',
    'trapper.apps.messaging',
    'trapper.apps.common',
    'trapper.apps.geomap',
    'trapper.apps.research',
    'trapper.apps.extra_tables',
    'trapper.apps.crowdsourcing',

    'leaflet_storage',
    'djgeojson',
    'grappelli',
    'django.contrib.admin',
    'south',
    'django_extensions',
    'tinymce',
    'guardian',
    'fluent_comments',
    'crispy_forms',
    'djangular',
    'easy_thumbnails',
    'django_select2',
    'django_hstore',
    'bootstrap_pagination',
    #'debug_toolbar',
)

from django.contrib.messages import constants as messages

MESSAGE_TAGS = {
messages.ERROR: 'danger',
messages.DEBUG: 'info',
}

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
'version': 1,
'disable_existing_loggers': False,
'filters': {
'require_debug_false': {
'()': 'django.utils.log.RequireDebugFalse'
}
},
'handlers': {
'mail_admins': {
'level': 'ERROR',
'filters': ['require_debug_false'],
'class': 'django.utils.log.AdminEmailHandler'
}
},
'loggers': {
'django.request': {
'handlers': ['mail_admins'],
'level': 'ERROR',
'propagate': True,
},
}
}

# GUARDIAN SETTINGS
ANONYMOUS_USER_ID = -1
GUARDIAN_RENDER_403 = True

# FLUENT COMMENTS
FLUENT_COMMENTS_EXCLUDE_FIELDS = ('name', 'email', 'url')
COMMENTS_APP = 'fluent_comments'

# EASY THUMBNAILS
THUMBNAIL_BASEDIR = 'thumbs'
THUMBNAIL_ALIASES = {
    '': {
        'small': {'size': (80, 80), },
        'default': {'size': (100, 100), },
        'carousel': {'size': (150, 150), },
        'medium': {'size': (250, 250), },
        'large': {'size': (400, 400), },
        'video': {'size': (640, 480), },
    },
}

THUMBNAIL_SOURCE_GENERATORS = (
    'easy_thumbnails.source_generators.pil_image',  # Default
    'trapper.tools.video_thumb_source_generator.ffmpeg_frame',
)

# CRISPY FORMS
CRISPY_TEMPLATE_PACK = 'bootstrap3'
CRISPY_FAIL_SILENTLY = not DEBUG

#DJANGO-SELECT2
SELECT2_BOOTSTRAP = True
#AUTO_RENDER_SELECT2_STATICS = False

#SOUTH_DATABASE_ADAPTERS = {'default': 'south.db.postgresql_psycopg2'}

from trapper.apps.media_classification.widgets import VideoTimeRangeField
from django import forms
# predefined, simple attributes configuration
PREDEFINED_ATTRIBUTES_SIMPLE = {
    'annotations': {
        'label': 'Annotations',
        'help_text': 'Use annotations to classify precisly partial contents of your media files (at the moment available only for videos)',
        'formfield': VideoTimeRangeField,
    },
    'comments': {
        'label': 'Comments',
        'help_text': 'Simple Comments Field (TextArea)',
        'formfield': forms.CharField,
        'widget': forms.Textarea(attrs={'cols': 10, 'rows': 3}),
    },
}

# predefined, model-based attributes configuration
PREDEFINED_ATTRIBUTES_MODELS = {
    'species': {
        'app': 'extra_tables',
        'label': 'Species (Mammals of the world)',
        'choices_labels': 'english_name',
        'search_fields': ['english_name__icontains', 'latin_name__icontains'],
        'filters': ['family', 'genus'],
    },
}
