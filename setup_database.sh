#!/bin/bash
# Sets up the database with the default data (init_db.py) as well as a dummy data (debug_db.py)

python manage.py syncdb --noinput
python manage.py migrate
python manage.py shell_plus < trapper/scripts/db_basic.py
python manage.py shell_plus < trapper/scripts/db_test.py
